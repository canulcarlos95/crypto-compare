import React from "react";
import "./DisplayQuotation.css";

function DisplayQuotation({ quotationInfo }) {
  console.log(quotationInfo);
  return (
    <div className="row result">
      <div className="col-12 price">
        <span>Precio Actual: {quotationInfo.PRICE}</span>
      </div>
      <div className="col-12">
        <span>Precio más alto del día: {quotationInfo.HIGHDAY}</span>
      </div>
      <div className="col-12">
        <span>Precio más bajo del día: {quotationInfo.LOWDAY}</span>
      </div>
      <div className="col-12">
        <span>Variación últimas 24h: {quotationInfo.CHANGE24HOUR}</span>
      </div>
      <div className="col-12">
        <span>Última actualización: {quotationInfo.LASTUPDATE}</span>
      </div>
    </div>
  );
}

export default DisplayQuotation;
