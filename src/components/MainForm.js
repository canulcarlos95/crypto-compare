import React, { useState, useEffect, useRef } from "react";
import "./MainForm.css";
import DisplayQuotation from "./DisplayQuotation";
import CurrenciesProvider from "../service-providers/CurrenciesProvider";
import QuotationProvider from "../service-providers/QuotationProvider";
import SelectComponent from "./SelectComponent";
function MainForm() {
  const CURRENCIES = [
    { code: "USD", description: "Dolar Estados Unidos" },
    { code: "MXN", description: "Peso Mexicano" },
    { code: "EUR", description: "Euro" },
  ];

  const [cryptoInfo, setCryptoInfo] = useState([]);
  const [currencyId, setCurrencyId] = useState("");
  const [displayQuotation, setDisplayQuotation] = useState(false);
  const [quotation, setQuotation] = useState([]);
  const coin = useRef(null);
  const cryptoCoin = useRef(null);

  useEffect(() => {
    CurrenciesProvider(currencyId, setCryptoInfo);
  }, [currencyId, setCryptoInfo]);

  const getQoutation = () => {
    const cryptoCoinVal = cryptoCoin.current.value;
    const coinVal = coin.current.value;
    if (cryptoCoinVal !== "" && coinVal !== "") {
      QuotationProvider(
        cryptoCoinVal,
        coinVal,
        setQuotation,
        setDisplayQuotation
      );
    } else {
      setDisplayQuotation(false);
    }
  };

  return (
    <div className="container">
      <form>
        <div className="form-group">
          <label>SELECCIONAR MONEDA</label>
          <SelectComponent
            data={CURRENCIES}
            onChange={(e) => setCurrencyId(e.target.value)}
            reference={coin}
            firstOptionText="Selecciona una moneda"
            type="currency"
          />
        </div>
        <div className="form-group">
          <label>ELIGE TU CRIPTO MONEDA</label>
          <SelectComponent
            data={cryptoInfo}
            reference={cryptoCoin}
            firstOptionText="Selecciona una cripto moneda"
            type="crypto"
          />
        </div>
        <div className="form-group">
          <input
            type="submit"
            value="Cotizar"
            className="col-12 btn btn-info"
            onClick={(e) => {
              e.preventDefault();
              getQoutation();
            }}
          />
        </div>
      </form>
      {displayQuotation ? <DisplayQuotation quotationInfo={quotation} /> : null}
    </div>
  );
}

export default MainForm;
