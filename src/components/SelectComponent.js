import React from "react";

function SelectComponent({ data, onChange, reference, firstOptionText, type }) {
  return (
    <select className="form-control" onChange={onChange} ref={reference}>
      <option key="0" value="">
        {firstOptionText}
      </option>
      {data.map((data) => {
        if (type === "currency") {
          return (
            <option key={data.code} value={data.code}>
              {data.description}
            </option>
          );
        } else {
          return (
            <option key={data.Id} value={data.Name}>
              {data.FullName}
            </option>
          );
        }
      })}
    </select>
  );
}

export default SelectComponent;
