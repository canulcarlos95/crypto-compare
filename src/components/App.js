import React from "react";
import MainForm from "./MainForm";

function App() {
  return (
    <div className="row">
      <div className="nav justify-content-center main-container-crypto">
        <div className="col-6">
          <img
            src="/assets/cryptomonedas.png"
            className="img-fluid"
            alt="Responsive"
          />
        </div>
        <div className="col-6">
          <h1>COTIZADOR DE CRIPTOMONEDAS</h1>
          <MainForm />
        </div>
      </div>
    </div>
  );
}

export default App;
