import axios from "axios";

function CurrenciesProvider(currencyId, setCryptoInfo) {
  axios
    .get(
      "https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=" +
        currencyId
    )
    .then(function (response) {
      if (response.data.Response !== "Error") {
        const coinInfo = response.data.Data.map((coins) => {
          return coins.CoinInfo;
        });
        setCryptoInfo(coinInfo);
      }
    })
    .catch(function (error) {
      return error;
    });
}

export default CurrenciesProvider;
