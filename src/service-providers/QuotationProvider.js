import axios from "axios";

function QuotationProvider(
  cryptoCoin,
  coin,
  setQuotation,
  setDisplayQuotation
) {
  axios
    .get(
      `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${cryptoCoin}&tsyms=${coin}`
    )
    .then(function (response) {
      setQuotation(response.data.DISPLAY[cryptoCoin][coin]);
      setDisplayQuotation(true);
    })
    .catch(function (error) {
      return error;
    });
}

export default QuotationProvider;
